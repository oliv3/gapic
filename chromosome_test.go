package main

import (
	"testing"
)

var singleCrossoverAuxSuccessTestCases = []struct {
	name           string
	parent1        Chromosome
	parent2        Chromosome
	crossoverPoint int
	child1         Chromosome
	child2         Chromosome
}{
	{
		name:           "crossover point at the beginning",
		parent1:        Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:        Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint: 1,
		child1:         Chromosome{0, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		child2:         Chromosome{10, 1, 2, 3, 4, 5, 6, 7, 8, 9},
	},
	{
		name:           "crossover point in the middle",
		parent1:        Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:        Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint: 5,
		child1:         Chromosome{0, 1, 2, 3, 4, 15, 16, 17, 18, 19},
		child2:         Chromosome{10, 11, 12, 13, 14, 5, 6, 7, 8, 9},
	},
	{
		name:           "crossover point at the end",
		parent1:        Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:        Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint: 9,
		child1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 19},
		child2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 9},
	},
}

func TestSingleCrossoverAuxSuccess(t *testing.T) {
	for _, tt := range singleCrossoverAuxSuccessTestCases {
		child1, child2, err := singleCrossoverAux(tt.parent1, tt.parent2, len(tt.parent1), tt.crossoverPoint)
		if nil != err {
			t.Errorf("singleCrossoverAux return an error: %s", err.Error())
		}
		if !child1.equals(tt.child1) {
			t.Errorf("first children mismatch: expected %v, got %v", child1, tt.child1)
		}
		if !child2.equals(tt.child2) {
			t.Errorf("second children mismatch: expected %v, got %v", child2, tt.child2)
		}
	}
}

var singleCrossoverAuxFailureTestCases = []struct {
	name           string
	parent1        Chromosome
	parent2        Chromosome
	crossoverPoint int
}{
	{
		name:           "invalid beginning",
		parent1:        Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:        Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint: 0,
	},
	{
		name:           "invalid end",
		parent1:        Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:        Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint: 10,
	},
}

func TestSingleCrossoverAuxFailure(t *testing.T) {
	for _, tt := range singleCrossoverAuxFailureTestCases {
		child1, child2, err := singleCrossoverAux(tt.parent1, tt.parent2, len(tt.parent1), tt.crossoverPoint)
		if nil == err {
			t.Errorf("singleCrossoverAux returned %v, %v but was expected to fail", child1, child2)
		}
	}
}

var doubleCrossoverAuxSuccessTestCases = []struct {
	name            string
	parent1         Chromosome
	parent2         Chromosome
	crossoverPoint1 int
	crossoverPoint2 int
	child1          Chromosome
	child2          Chromosome
}{
	{
		name:            "crossover points at the beginning",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 1,
		crossoverPoint2: 2,
		child1:          Chromosome{0, 11, 2, 3, 4, 5, 6, 7, 8, 9},
		child2:          Chromosome{10, 1, 12, 13, 14, 15, 16, 17, 18, 19},
	},
	{
		name:            "crossover points in the middle",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 4,
		crossoverPoint2: 6,
		child1:          Chromosome{0, 1, 2, 3, 14, 15, 6, 7, 8, 9},
		child2:          Chromosome{10, 11, 12, 13, 4, 5, 16, 17, 18, 19},
	},
	{
		name:            "crossover points at the end",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 8,
		crossoverPoint2: 9,
		child1:          Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 18, 9},
		child2:          Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 8, 19},
	},
}

func TestDoubleCrossoverAux(t *testing.T) {
	for _, tt := range doubleCrossoverAuxSuccessTestCases {
		child1, child2, err := doubleCrossoverAux(tt.parent1, tt.parent2, len(tt.parent1), tt.crossoverPoint1, tt.crossoverPoint2)
		if nil != err {
			t.Errorf("doubleCrossoverAux return an error: %s", err.Error())
		}
		if !child1.equals(tt.child1) {
			t.Errorf("first children mismatch: expected %v, got %v", child1, tt.child1)
		}
		if !child2.equals(tt.child2) {
			t.Errorf("second children mismatch: expected %v, got %v", child2, tt.child2)
		}
	}
}

var doubleCrossoverAuxFailureTestCases = []struct {
	name            string
	parent1         Chromosome
	parent2         Chromosome
	crossoverPoint1 int
	crossoverPoint2 int
}{
	{
		name:            "null segment",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 5,
		crossoverPoint2: 5,
	},
	{
		name:            "first crossover point after the second",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 6,
		crossoverPoint2: 5,
	},
	{
		name:            "invalid beginning",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 0,
		crossoverPoint2: 1,
	},
	{
		name:            "invalid end",
		parent1:         Chromosome{0, 1, 2, 3, 4, 5, 6, 7, 8, 9},
		parent2:         Chromosome{10, 11, 12, 13, 14, 15, 16, 17, 18, 19},
		crossoverPoint1: 9,
		crossoverPoint2: 10,
	},
}

func TestDoubleCrossoverAuxFailure(t *testing.T) {
	for _, tt := range doubleCrossoverAuxFailureTestCases {
		child1, child2, err := doubleCrossoverAux(tt.parent1, tt.parent2, len(tt.parent1), tt.crossoverPoint1, tt.crossoverPoint2)
		if nil == err {
			t.Errorf("singleCrossoverAux returned %v, %v but was expected to fail", child1, child2)
		}
	}
}
