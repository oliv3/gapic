package main

import (
	"encoding/json"
	"log"
)

// Mutations counts the types of mutations that occured
type Mutations struct {
	RandomizeOne    uint64 `json:"randomizeOne"`
	RandomizeSome   uint64 `json:"randomizeSome"`
	DecrementOne    uint64 `json:"decrementOne"`
	DecrementSome   uint64 `json:"decrementSome"`
	DecrementAll    uint64 `json:"decrementAll"`
	IncrementOne    uint64 `json:"incrementOne"`
	IncrementSome   uint64 `json:"incrementSome"`
	IncrementAll    uint64 `json:"incrementAll"`
	InvertOne       uint64 `json:"invertOne"`
	InvertSome      uint64 `json:"invertSome"`
	InvertAll       uint64 `json:"invertAll"`
	SwapTwoAdjacent uint64 `json:"swapTwoAdjacent"`
	SwapTwoDistant  uint64 `json:"swapTwoDistant"`
	AverageOne      uint64 `json:"averageOne"`
	AverageSome     uint64 `json:"averageSome"`
	BslOne          uint64 `json:"bslOne"`
	BslSome         uint64 `json:"bslSome"`
	BslAll          uint64 `json:"bslAll"`
	BsrOne          uint64 `json:"bsrOne"`
	BsrSome         uint64 `json:"bsrSome"`
	BsrAll          uint64 `json:"bsrAll"`
	AddOne          uint64 `json:"addOne"`
	AddSome         uint64 `json:"addSome"`
	AddAll          uint64 `json:"addAll"`
	SubOne          uint64 `json:"subOne"`
	SubSome         uint64 `json:"subSome"`
	SubAll          uint64 `json:"subAll"`
}

var mutationsCount Mutations

// Pixels counts how many pixel are ok/low/high
type Pixels struct {
	Ok        uint32 `json:"ok"`
	TooDark   uint32 `json:"tooDark"`
	TooBright uint32 `json:"tooBright"`
}

// Stats defines the statistics
type Stats struct {
	SimulationStarted int64     `json:"simulationStarted"`
	SimulationEnded   int64     `json:"simulationEnded"`
	Generation        uint64    `json:"generation"`
	TotalFitness      uint64    `json:"totalFitness"`
	BestFitness       uint64    `json:"bestFitness"`
	BestScore         float64   `json:"bestScore"`
	Mutations         Mutations `json:"mutations"`
	Pixels            Pixels    `json:"pixels"`
}

func (pop *Population) stats() *Stats {
	var pixels Pixels

	best := pop.Phenotypes[0]
	for i, g := range best.Chromosome {
		if g == solution[i] {
			pixels.Ok++
		} else if g < solution[i] {
			pixels.TooDark++
		} else {
			pixels.TooBright++
		}
	}

	return &Stats{
		SimulationStarted: pop.SimulationStarted,
		SimulationEnded:   pop.SimulationEnded,
		Generation:        pop.Generation,
		TotalFitness:      pop.TotalFitness,
		BestFitness:       pop.Phenotypes[0].Fitness,
		BestScore:         float64(best.Fitness) / float64(maxScore),
		Mutations:         mutationsCount,
		Pixels:            pixels,
	}
}

func (h *Hub) sendStats(stats *Stats) {
	if json, err := json.Marshal(stats); nil == err {
		h.broadcast <- json
	} else {
		log.Fatal(err.Error())
	}
}
