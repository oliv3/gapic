#!/usr/bin/env bash
echo -n "Encoding video... "
ffmpeg -hide_banner -loglevel error -y -r 60 -f image2 -i out/out-%09d.png -vcodec libx264 -crf 25 -pix_fmt yuv420p gapic_new.mp4
echo done.
mv -f gapic_new.mp4 gapic.mp4
