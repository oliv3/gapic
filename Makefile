all:
	go build

nosdl2:
	CGO_ENABLED=0 go build -tags without_sdl2

clean:
	rm -f gapic

run:
	GOGC=50 ./gapic
