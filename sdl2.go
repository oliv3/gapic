// +build !without_sdl2

package main

import (
	"math"
	"unsafe"

	"github.com/veandco/go-sdl2/sdl"
)

const zoom = 5

var (
	window       *sdl.Window
	targetImage  *sdl.Surface
	windowWidth  int32 = width * 3 * zoom
	windowHeigth int32 = height * zoom
)

func initSDL2(ec chan<- interface{}) {
	var err error

	if err = sdl.Init(sdl.INIT_VIDEO); nil != err {
		panic(err.Error())
	}

	if window, err = sdl.CreateWindow("GAPic", 50, 50, windowWidth, windowHeigth, sdl.WINDOW_SHOWN|sdl.WINDOW_RESIZABLE); nil != err {
		panic(err.Error())
	}

	targetImage, err = sdl.CreateRGBSurfaceWithFormatFrom(unsafe.Pointer(&solution[0]), width, height, 24, width*3, sdl.PIXELFORMAT_RGB24)
	if nil != err {
		panic(err.Error())
	}
	go pollEvents(ec)
}

func run(ch <-chan Chromosome, ec chan<- interface{}) {
	var err error
	initSDL2(ec)
	defer sdl.Quit()
	defer window.Destroy()
	defer targetImage.Free()

	for chr := range ch {
		var surface *sdl.Surface
		if surface, err = window.GetSurface(); nil != err {
			panic(err.Error())
		}
		defer surface.Free()

		err = targetImage.BlitScaled(nil, surface, &sdl.Rect{X: windowWidth * 2 / 3, Y: 0, W: windowWidth / 3, H: windowHeigth})
		if nil != err {
			panic(err.Error())
		}

		if chrImage, err := sdl.CreateRGBSurfaceWithFormatFrom(unsafe.Pointer(&chr[0]), width, height, 24, width*3, sdl.PIXELFORMAT_RGB24); nil == err {
			defer chrImage.Free()
			err = chrImage.BlitScaled(nil, surface, &sdl.Rect{X: 0, Y: 0, W: windowWidth / 3, H: windowHeigth})
			if nil != err {
				panic(err.Error())
			}
		}

		delta := make([]byte, chromosomeLen)
		for i, g := range chr {
			if g == solution[i] {
				delta[i] = g
			} else {
				delta[i] = byte(math.Abs(float64(int(g) - int(solution[i]))))
			}
		}

		if deltaImage, err := sdl.CreateRGBSurfaceWithFormatFrom(unsafe.Pointer(&delta[0]), width, height, 24, width*3, sdl.PIXELFORMAT_RGB24); nil == err {
			defer deltaImage.Free()
			err = deltaImage.BlitScaled(nil, surface, &sdl.Rect{X: windowWidth / 3, Y: 0, W: windowWidth / 3, H: windowHeigth})
			if nil != err {
				panic(err.Error())
			}
		}

		window.UpdateSurface()
	}
}

func pollEvents(ec chan<- interface{}) {
	running := true

	for running {
		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch t := event.(type) {
			case *sdl.QuitEvent:
				running = false
			case *sdl.WindowEvent:
				if t.Event == 5 && t.Data1 > 0 && t.Data2 > 0 {
					windowWidth = t.Data1
					windowHeigth = t.Data2
				}
			}
		}
		sdl.Delay(100)
	}
	// notify main function to exit
	ec <- struct{}{}
}
