package main

import (
	"fmt"
	"math"
	"math/rand"
)

// Chromosome defines a chromosome
type Chromosome []uint8

func newChromosome(size int) Chromosome {
	chromosome := make([]uint8, size)
	rand.Read(chromosome)

	return chromosome
}

func duplicateChromosome(chrom Chromosome) Chromosome {
	chromosome := make([]uint8, len(chrom))
	copy(chromosome, chrom)

	return chromosome
}

func (chrom Chromosome) String() string {
	s := "[ "

	for i, v := range chrom {
		s += fmt.Sprintf("%03d", v)
		if i != len(chrom)-1 {
			s += " "
		}
	}

	return s + " ]"
}

func (chrom Chromosome) evaluate() uint64 {
	var fitness uint64

	for i, g := range chrom {
		var delta int32 = int32(g) - int32(solution[i])
		var score uint32 = uint32(255 - math.Abs(float64(delta)))
		// pixels too far away from the solution get 0 points
		if score >= 128 {
			fitness += uint64(score * score)
		}
	}

	return fitness
}

func (chrom Chromosome) equals(chrom2 Chromosome) bool {
	if len(chrom) != len(chrom2) {
		return false
	}
	for i, a := range chrom {
		if a != chrom2[i] {
			return false
		}
	}
	return true
}
