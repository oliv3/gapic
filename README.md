# Introduction
GAPic is an attempt to do image reconstruction with a Genetic Algorithm.
The test image used is a 100x100 8bits grayscale image.
Go looked like a good candidate for practising with concurrency, so there are goroutines
everywhere :)

# Requirements
- libsdl2-dev
- libsdl2-image-dev

# Create the ramdisk
```
$ ./scripts/ramdisk.sh
```

# Build
## SDL2 version
```
$ make
```
## without SDL2
```
$ make nosdl2
```

# Run
```
$ make run
```
**Note:** if you run the binary directly, make sure you have set the `GOGC` environment
variable, e.g: `export GOGC=50` or you'll have serious memory issues !

# Web interface
You can see the algorithm's progress on `http://localhost:58165`.