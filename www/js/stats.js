let lastTotalFitness = 0
let totalFitness = 0

const addFitness = fitness => {
  lastTotalFitness = totalFitness
  totalFitness = fitness
  if (lastTotalFitness) {
    fitnessDelta = totalFitness - lastTotalFitness
    const fitnessDeltaText = '&#859' + ((fitnessDelta > 0) ? '3' : '5') + ';' + Math.abs(fitnessDelta)
    $('#fitnessDelta').html(fitnessDeltaText)
    const color = (fitnessDelta < 0) ? 'red' : 'lime'
    $('#fitnessDelta').css({ color: color })
  }
}

let lastBestScore = 0
let bestScore = 0

const addArrow = value => `&#859${value > 0 ? '3' : '5'};` + Math.abs(value)

const addScore = score => {
  lastBestScore = bestScore
  bestScore = Math.floor(score * 10000) / 100
  const bestScoreText = bestScore.toFixed(2) + '%'
  $('#bestScore').html(bestScoreText)
  if (lastBestScore) {
    const delta = Math.floor((bestScore - lastBestScore) * 1000) / 1000
    if (delta) {
      const color = delta < 0 ? 'red' : 'lime'
      $('#scoreDelta').html(addArrow(Math.abs(delta).toFixed(2)))
      $('#scoreDelta').css({ color })
    } else {
      $('#scoreDelta').html('&#8213;'.repeat(5))
      $('#scoreDelta').css({ color: 'white' })
    }
  }
}

let lastOk = 0
let lastTooDark = 0
let lastTooBright = 0

const deltaColor = delta => !delta ? 'white' : (delta > 0 ? 'lime' : 'red')

const addPixels = pixels => {
  const { ok, tooDark, tooBright } = pixels
  $('#ok').html(ok)
  $('#tooDark').html(tooDark)
  $('#tooBright').html(tooBright)
  deltaOk = ok - lastOk
  deltaTooDark = tooDark - lastTooDark
  deltaTooBright = tooBright - lastTooBright
  const noDelta = '&#8213;'.repeat(2)
  if (lastOk) {
    $('#deltaOk').html(deltaOk ? addArrow(deltaOk) : noDelta)
    $('#deltaTooDark').html(deltaTooDark ? addArrow(deltaTooDark) : noDelta)
    $('#deltaTooBright').html(deltaTooBright ? addArrow(deltaTooBright) : noDelta)
    $('#deltaOk').css({ color: deltaColor(deltaOk) })
    $('#deltaTooDark').css({ color: deltaColor(deltaTooDark) })
    $('#deltaTooBright').css({ color: deltaColor(deltaTooBright) })
  }
  lastOk = ok
  lastTooDark = tooDark
  lastTooBright = tooBright
}
