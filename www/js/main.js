const main = () => {
  initHost()
  connectWs()
  $('#vidBox').VideoPopUp({
    backgroundColor: "#17212a",
    opener: "video1",
    idvideo: "v1"
  })
  $('#replayGeneration').html(`Generation ${replayGeneration}`)
  $('#replaySlider').slider({
    min: 1,
    max: 1,
    values: 1,
    slide: function (event, ui) {
      $('#current').attr('src', `/replay?generation=${ui.value}`)
      $('#replayGeneration').html(`Generation ${ui.value}`)
    }
  })
  $('#replaySliderDiv').hide()
}

let replayMode = false
let replayGeneration = 1

const replay = event => {
  replayMode = event.target.checked
  if (replayMode) {
    $('#statsTable').hide()
    $('#replaySliderDiv').show()
    $('#current').attr('src', '/replay?generation=1')
    $('#replayGeneration').html("Generation 1")
    $('#replaySlider').slider({ value: 1 })
  } else {
    $('#statsTable').show()
    $('#replaySliderDiv').hide()
  }
}