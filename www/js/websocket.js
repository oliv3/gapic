let wsHost = JSON.parse(localStorage.getItem('wsHost')) || 'localhost'
const wsPort = 58165
let ws = null

const initHost = () => {
  $('#wsHost').val(wsHost)
}

const hostChanged = event => {
  console.log(event.target.value)
  wsHost = event.target.value
  localStorage.setItem('wsHost', JSON.stringify(wsHost))
  if (ws !== null) {
    $('#connected').html('Disconnected')
    ws.close()
  }
}

const connectWs = () => {
  const wsUrl = `ws://${wsHost}:${wsPort}/ws`
  console.log(`Connecting to ${wsUrl}`)
  ws = new WebSocket(wsUrl)

  ws.onopen = function () {
    console.info('Connected')
    $('#connected').html('Connected')
    ws.send('getStats')
  }

  ws.onmessage = function (event) {
    // console.log('event.data', event.data)
    const payload = JSON.parse(event.data)
    // console.log('payload', payload)
    process(payload)
  }

  ws.onclose = function (error) {
    $('#connected').html('Disconnected')
    ws = null
    setTimeout(function () {
      connectWs()
    }, 1000)
  }

  ws.onerror = function (error) {
    // console.error('Socket encountered error: ', error, 'Closing socket')
    ws.close()
  }
}

const process = stats => {
  // console.log("stats", stats)
  $('#generation').html(stats.generation)
  $('#replaySlider').slider({ max: stats.generation })
  const POPSIZE = 25000
  $('#tries').html(stats.generation * POPSIZE)
  const ts = new Date().getTime()
  if (!replayMode) {
    $('#current').attr('src', `current.png?t=${ts}`)
  }
  addFitness(stats.totalFitness)
  addScore(stats.bestScore)
  addPixels(stats.pixels)
  let formatted = 'Simulation started '
  formatted += moment.unix(stats.simulationStarted).format('MMMM Do YYYY, hh:mm:ss a')
  formatted += ` (${moment.unix(stats.simulationStarted).fromNow()})`
  if (stats.simulationEnded) {
    formatted += `. Simulation ended `
    formatted += moment.unix(stats.simulationEnded).format('MMMM Do YYYY, hh:mm:ss a')
    formatted += ` (${moment.unix(stats.simulationEnded).fromNow()})`
    const simulationTime = stats.simulationEnded - stats.simulationStarted
    const duration = moment.duration(simulationTime, 'seconds')
    formatted += `. Solution found in ${duration.humanize()}.`
  }
  $('#simulationStatus').text(formatted)
}
