package main

import "math"

// Phenotype defines a member of the population
type Phenotype struct {
	Chromosome Chromosome
	Fitness    uint64 `json:"-"`
}

func (p *Phenotype) at(index int) uint8 {
	return p.Chromosome[index]
}

func (p *Phenotype) channelAt(index int) uint8 {
	return uint8(math.Abs(float64(int(p.Chromosome[index]) - int(solution[index]))))
}
