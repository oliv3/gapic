package main

import (
	"math/rand"
	"sync"
)

func (pop *Population) roulette() int {
	var index int
	num := rand.Uint64() % pop.TotalFitness
	var sum uint64 = 0

	for {
		sum += pop.Phenotypes[index].Fitness
		if sum >= num {
			return index
		}
		index++
	}
}

func (pop *Population) next() Population {
	nextPop := Population{
		SimulationStarted: pop.SimulationStarted,
		SimulationEnded:   pop.SimulationEnded,
		Generation:        pop.Generation + 1,
	}
	nextPop.Phenotypes = make([]Phenotype, popSize)

	wg := sync.WaitGroup{}
	wg.Add(popSize / 2)
	for i := 0; i < popSize-1; i += 2 {
		go func(index int) {
			defer wg.Done()
			p1 := 0
			if rand.Float32() < 0.5 {
				p1 = pop.roulette()
			}
			p2 := pop.roulette()

			for p1 == p2 {
				p2 = pop.roulette()
			}

			parent1 := pop.Phenotypes[p1]
			parent2 := pop.Phenotypes[p2]
			cross := randomInt(0, 3)
			var child1, child2 Chromosome
			var err error
			switch cross {
			case 0:
				child1, child2, err = singleCrossover(parent1.Chromosome, parent2.Chromosome, chromosomeLen)
				if nil != err {
					panic(err.Error())
				}

			case 1:
				child1, child2, err = doubleCrossover(parent1.Chromosome, parent2.Chromosome, chromosomeLen)
				if nil != err {
					panic(err.Error())
				}

			case 2:
				child1, child2 = uniformCrossover(parent1.Chromosome, parent2.Chromosome)
			}
			nextPop.Phenotypes[index].Chromosome = child1
			nextPop.Phenotypes[index+1].Chromosome = child2
		}(i)
	}
	wg.Wait()

	return nextPop
}
