package main

import (
	"fmt"
	"math/rand"
)

func singleCrossover(parent1, parent2 Chromosome, size int) (Chromosome, Chromosome, error) {
	p := rand.Intn(int(size-1)) + 1
	return singleCrossoverAux(parent1, parent2, size, p)
}

func singleCrossoverAux(parent1, parent2 Chromosome, size int, p int) (Chromosome, Chromosome, error) {
	if p <= 0 || p >= size {
		return nil, nil, fmt.Errorf("invalid crossover point %d", p)
	}

	child1 := duplicateChromosome(parent1)
	child2 := duplicateChromosome(parent2)

	copy(child1[p:size], parent2[p:size])
	copy(child2[p:size], parent1[p:size])

	return child1, child2, nil
}

// 01234 size = 4
//  xx
//   yy
// 0123456789 size = 10
//  x      x
func doubleCrossover(parent1, parent2 Chromosome, size int) (Chromosome, Chromosome, error) {
	p1 := randomInt(1, size-2)
	p2 := randomInt(p1+1, size-1)
	return doubleCrossoverAux(parent1, parent2, size, p1, p2)
}

func doubleCrossoverAux(parent1, parent2 Chromosome, size, p1, p2 int) (Chromosome, Chromosome, error) {
	if p1 <= 0 || p1 >= p2 || p2 >= size {
		return nil, nil, fmt.Errorf("invalid crossover points %d %d", p1, p2)
	}

	child1 := duplicateChromosome(parent1)
	child2 := duplicateChromosome(parent2)

	copy(child1[p1:p2], parent2[p1:p2])
	copy(child2[p1:p2], parent1[p1:p2])

	return child1, child2, nil
}

func uniformCrossover(parent1, parent2 Chromosome) (Chromosome, Chromosome) {
	size := len(parent1)
	if len(parent2) != size {
		panic("different sizes")
	}
	child1 := make([]uint8, size)
	child2 := make([]uint8, size)

	for i, v := range parent1 {
		if rand.Float32() < 0.5 {
			child1[i] = v
			child2[i] = parent2[i]
		} else {
			child1[i] = parent2[i]
			child2[i] = v
		}
	}

	return child1, child2
}
