module gitlab.com/oliv3/gapic

go 1.15

require (
	github.com/gorilla/websocket v1.4.2
	github.com/veandco/go-sdl2 v0.4.10
)
