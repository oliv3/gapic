package main

import (
	"math/rand"
	"sync"
)

type mutation func(Chromosome) Chromosome

const (
	nbMutations = 27
	// maximum number of changes in "*Some" mutations
	maxSize = chromosomeLen >> 2 // max 25%
)

var mutations = [nbMutations]mutation{
	randomizeOne,
	randomizeSome,
	decrementOne,
	decrementSome,
	decrementAll,
	incrementOne,
	incrementSome,
	incrementAll,
	invertOne,
	invertSome,
	invertAll,
	swapTwoAdjacent,
	swapTwoDistant,
	averageOne,
	averageSome,
	bslOne,
	bslSome,
	bslAll,
	bsrOne,
	bsrSome,
	bsrAll,
	addOne,
	addSome,
	addAll,
	subOne,
	subSome,
	subAll,
}

func randomInt(min, max int) int {
	return rand.Intn(max-min) + min
}

func randomIndex() int {
	return rand.Intn(chromosomeLen)
}

func randomUint8() uint8 {
	return uint8(rand.Intn(256))
}

func randomMutation() int {
	return randomInt(0, nbMutations)
}

// used in add* and sub* mutations
func randomValue() uint8 {
	return uint8(2 + rand.Intn(9)) // 2..10
}

func (pop *Population) mutate() *Population {
	wg := sync.WaitGroup{}
	wg.Add(popSize)
	for i := range pop.Phenotypes {
		go func(i int) {
			defer wg.Done()
			if rand.Float32() < mutationProbability {
				mutation := randomMutation()
				pop.Phenotypes[i].Chromosome = mutations[mutation](pop.Phenotypes[i].Chromosome)
			}
		}(i)
	}
	wg.Wait()

	return pop
}

func randomizeOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] randomizeOne")
	mutationsCount.RandomizeOne++
	mutated := duplicateChromosome(chrom)

	mutated.randomize()

	return mutated
}

func (chrom Chromosome) randomize() {
	index := randomIndex()
	old := chrom[index]

	for {
		new := randomUint8()
		if old != new {
			chrom[index] = new
			break
		}
	}
}

func randomizeSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] randomizeSome")
	mutationsCount.RandomizeSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.randomize()
	}

	return mutated
}

func decrementOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] decrementOne")
	mutationsCount.DecrementOne++
	mutated := duplicateChromosome(chrom)

	mutated.decrement(randomIndex())

	return mutated
}

func (chrom Chromosome) decrement(index int) {
	chrom[index]--
}

func decrementSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] decrementSome")
	mutationsCount.DecrementSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.decrement(randomIndex())
	}

	return mutated
}

func decrementAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] decrementAll")
	mutationsCount.DecrementAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.decrement(i)
	}

	return mutated
}

func incrementOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] incrementOne")
	mutationsCount.IncrementOne++
	mutated := duplicateChromosome(chrom)

	mutated.increment(randomIndex())

	return mutated
}

func (chrom Chromosome) increment(index int) {
	chrom[index]++
}

func incrementSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] incrementSome")
	mutationsCount.IncrementSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.increment(randomIndex())
	}

	return mutated
}

func incrementAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] incrementAll")
	mutationsCount.IncrementAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.increment(i)
	}

	return mutated
}

func invertOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] invertOne")
	mutationsCount.InvertOne++
	mutated := duplicateChromosome(chrom)

	mutated.invert(randomIndex())

	return mutated
}

func (chrom Chromosome) invert(index int) {
	chrom[index] = 255 - chrom[index]
}

func invertSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] invertSome")
	mutationsCount.InvertSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.invert(randomIndex())
	}

	return mutated
}

func invertAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] invertAll")
	mutationsCount.InvertAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.invert(i)
	}

	return mutated
}

func swapTwoAdjacent(chrom Chromosome) Chromosome {
	// fmt.Println("[m] swapTwoAdjacent")
	mutationsCount.SwapTwoAdjacent++
	mutated := duplicateChromosome(chrom)

	index := rand.Intn(chromosomeLen - 1)
	mutated[index], mutated[index+1] = mutated[index+1], mutated[index]

	return mutated
}

func swapTwoDistant(chrom Chromosome) Chromosome {
	// fmt.Println("[m] swapTwoDistant")
	mutationsCount.SwapTwoDistant++
	mutated := duplicateChromosome(chrom)

	index1 := randomIndex()
	index2 := randomIndex()
	for index2 == index1 || index2 == index1-1 || index2 == index1+1 {
		index2 = randomIndex()
	}

	mutated[index1], mutated[index2] = mutated[index2], mutated[index1]

	return mutated
}

func randomAveragePosition() int {
	return randomInt(width+1, chromosomeLen-width-1)
}

func averageOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] averageOne")
	mutationsCount.AverageOne++
	mutated := duplicateChromosome(chrom)

	mutated.average(randomAveragePosition())

	return mutated
}

// average returns the average of the 8 surrounding pixels
// p is in [width+1..chromosomeLen-width-1[
func (chrom Chromosome) average(p int) {
	// middle line
	var s uint32
	s += uint32(chrom[p-1])
	s += uint32(chrom[p+1])

	// top line
	s += uint32(chrom[p-width-1])
	s += uint32(chrom[p-width])
	s += uint32(chrom[p-width+1])

	// bottom line
	s += uint32(chrom[p+width-1])
	s += uint32(chrom[p+width])
	s += uint32(chrom[p+width+1])

	chrom[p] = uint8(s / 8)
}

func averageSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] averageSome")
	mutationsCount.AverageSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.average(randomAveragePosition())
	}

	return mutated
}

func bslOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bslOne")
	mutationsCount.BslOne++
	mutated := duplicateChromosome(chrom)

	mutated.bsl(randomIndex())

	return mutated
}

func (chrom Chromosome) bsl(index int) {
	chrom[index] <<= 1
}

func bslSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bslSome")
	mutationsCount.BslSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.bsl(randomIndex())
	}

	return mutated
}

func bslAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bslAll")
	mutationsCount.BslAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.bsl(i)
	}

	return mutated
}

func bsrOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bslOne")
	mutationsCount.BslOne++
	mutated := duplicateChromosome(chrom)

	mutated.bsr(randomIndex())

	return mutated
}

func (chrom Chromosome) bsr(index int) {
	chrom[index] >>= 1
}

func bsrSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bsrSome")
	mutationsCount.BsrSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.bsr(randomIndex())
	}

	return mutated
}

func bsrAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] bsrAll")
	mutationsCount.BsrAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.bsr(i)
	}

	return mutated
}

func addOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] addOne")
	mutationsCount.AddOne++
	mutated := duplicateChromosome(chrom)

	mutated.add(randomIndex())

	return mutated
}

func (chrom Chromosome) add(index int) {
	chrom[index] += randomValue()
}

func addSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] addSome")
	mutationsCount.AddSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.add(randomIndex())
	}

	return mutated
}

func addAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] addAll")
	mutationsCount.AddAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.add(i)
	}

	return mutated
}

func subOne(chrom Chromosome) Chromosome {
	// fmt.Println("[m] subOne")
	mutationsCount.SubOne++
	mutated := duplicateChromosome(chrom)

	mutated.sub(randomIndex())

	return mutated
}

func (chrom Chromosome) sub(index int) {
	chrom[index] -= randomValue()
}

func subSome(chrom Chromosome) Chromosome {
	// fmt.Println("[m] subSome")
	mutationsCount.SubSome++
	mutated := duplicateChromosome(chrom)

	for i := 0; i <= rand.Intn(maxSize); i++ {
		mutated.sub(randomIndex())
	}

	return mutated
}

func subAll(chrom Chromosome) Chromosome {
	// fmt.Println("[m] subAll")
	mutationsCount.SubAll++
	mutated := duplicateChromosome(chrom)

	for i := range chrom {
		mutated.sub(i)
	}

	return mutated
}
