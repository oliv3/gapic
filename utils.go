package main

import (
	"encoding/json"
	"fmt"
	"image"
	"image/color"
	"image/png"
	"io"
	"io/ioutil"
	"os"
	"sync"
	"time"
)

const file = "ramdisk/population.json"
const backupFile = "ramdisk/population.json.old"
const bytesImagePath = "www/img/Lenna-RGB-100.bytes"

func loadImage() {
	var err error

	solution, err = ioutil.ReadFile(bytesImagePath)
	if nil != err {
		panic(err)
	}
}

func dumpPopulation(pop []byte) {
	_, err := os.Stat(file)
	if nil == err {
		source, err := os.Open(file)
		if nil != err {
			panic(err.Error())
		}
		defer source.Close()

		destination, err := os.Create(backupFile)
		if nil != err {
			panic(err.Error())
		}
		defer destination.Close()
		_, err = io.Copy(destination, source)
		if nil != err {
			panic(err.Error())
		}
	}

	err = ioutil.WriteFile(file, pop, 0644)
	if nil != err {
		panic(err.Error())
	}
}

func readPopulation() Population {
	pop := Population{}

	content, err := ioutil.ReadFile(file)
	if nil != err {
		fmt.Println("Creating a new population")
		pop.Phenotypes = make([]Phenotype, popSize)
		wg := sync.WaitGroup{}
		wg.Add(popSize)
		for i := range pop.Phenotypes {
			go func(i int) {
				defer wg.Done()
				pop.Phenotypes[i].Chromosome = newChromosome(chromosomeLen)
			}(i)
		}
		wg.Wait()
		pop.SimulationStarted = time.Now().Unix()

		return pop
	}
	fmt.Println("Loading existing population")
	err = json.Unmarshal(content, &pop)
	if nil != err {
		panic(err.Error())
	}

	return pop
}

const outDir = "out"
const padding = 10

func (pop *Population) writeProgress() {
	_, err := os.Stat(outDir)
	if nil != err {
		err := os.Mkdir(outDir, 0755)
		if nil != err {
			panic(err.Error())
		}
	}
	upLeft := image.Point{0, 0}
	lowRight := image.Point{width*3 + padding*4, height + padding*2}

	best := pop.Phenotypes[0]
	img := image.NewRGBA(image.Rectangle{upLeft, lowRight})
	black := color.NRGBA{0, 0, 0, 255}
	for y := 0; y < height+padding*2; y++ {
		for x := 0; x < width*3+padding*4; x++ {
			img.Set(x, y, black)
		}
	}
	idx := 0
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			img.Set(x+padding, y+padding, color.NRGBA{best.at(idx), best.at(idx + 1), best.at(idx + 2), 255})
			img.Set(width+padding*2+x, y+padding, color.NRGBA{best.channelAt(idx), best.channelAt(idx + 1), best.channelAt(idx + 2), 255})
			img.Set(width*2+padding*3+x, y+padding, color.NRGBA{solution[idx], solution[idx+1], solution[idx+2], 255})
			idx += 3
		}
	}
	pngFilename := fmt.Sprintf("out/out-%09d.png", pop.Generation)
	f, err := os.Create(pngFilename)
	if nil != err {
		panic(err.Error())
	}
	png.Encode(f, img)
	f.Close()
	os.Remove("current.png")
	os.Link(pngFilename, "current.png")
}
