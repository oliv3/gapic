package main

import (
	"log"
	"net/http"
)

var addr string = ":58165"

func initWebserver(hub *Hub) {
	initHttp()
	initWebsocket(hub)
	log.Fatal(http.ListenAndServe(addr, nil))
}
