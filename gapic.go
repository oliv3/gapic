// Package gapic recreates a picture using a genetic algorithm.
package main

import (
	"encoding/json"
	"fmt"
	"math/rand"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

const (
	popSize                    = 10000
	width                      = 100
	height                     = 100
	chromosomeLen              = width * height * 3
	mutationProbability        = 0.5 // 50%
	maxScore                   = chromosomeLen * 255 * 255
	maxGen              uint64 = 0
)

var (
	solution []byte
)

// Population is a set of Phenotypes
type Population struct {
	Phenotypes        []Phenotype
	TotalFitness      uint64 `json:"-"`
	Generation        uint64
	SimulationStarted int64
	SimulationEnded   int64
}

func (pop *Population) result() {
	fmt.Println("Generation", pop.Generation, "Fitness", pop.TotalFitness)
	for i := 0; i < 10; i++ {
		fmt.Println(i, pop.Phenotypes[i].Fitness, float64(pop.Phenotypes[i].Fitness)/float64(maxScore))
	}
}

func sep() {
	fmt.Println(strings.Repeat("==========", 8))
}

var pop Population

func SendStats(hub *Hub) {
	stats := pop.stats()
	hub.sendStats(stats)
}

func main() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)

	rand.Seed(time.Now().UnixNano())
	loadImage()

	ch := make(chan Chromosome)
	ec := make(chan interface{})
	go func(ec chan interface{}) {
		sig := <-sigs
		fmt.Println()
		fmt.Printf("Exiting on signal: %q\n", sig)
		ec <- struct{}{}
	}(ec)
	pop = readPopulation()

	pop.evaluate()
	go run(ch, ec)
	ch <- duplicateChromosome(pop.Phenotypes[0].Chromosome)
	pop.result()
	sep()

	hub := newHub()
	go hub.run()
	go initWebserver(hub)

	for maxGen == 0 || pop.Generation <= maxGen {
		pop = pop.next()
		pop.mutate()
		pop.evaluate()
		ch <- duplicateChromosome(pop.Phenotypes[0].Chromosome)
		pop.result()
		pop.writeProgress()

		SendStats(hub)

		if json, err := json.Marshal(pop); nil == err {
			dumpPopulation(json)
		}
		sep()

		// Stop simulation when 95% pixels are found
		if pop.Phenotypes[0].Fitness >= maxScore*.95 {
			pop.SimulationEnded = time.Now().Unix()
			fmt.Println("Solution found at generation", pop.Generation)
			SendStats(hub)
			close(ch)
			select {
			case _ = <-ec:
				goto exit
			}
		}

		select {
		case _ = <-ec:
			goto exit
		default:
			break
		}
	}
exit:
	fmt.Println("Bye!")
}
