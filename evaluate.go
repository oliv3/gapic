package main

import (
	"sort"
	"sync"
)

func (pop *Population) evaluate() {
	wg := sync.WaitGroup{}
	wg.Add(popSize)
	for i := range pop.Phenotypes {
		go func(i int) {
			defer wg.Done()
			pop.Phenotypes[i].Fitness = pop.Phenotypes[i].Chromosome.evaluate()
		}(i)
	}
	wg.Wait()

	sort.Slice(pop.Phenotypes, func(i, j int) bool {
		return pop.Phenotypes[i].Fitness > pop.Phenotypes[j].Fitness
	})

	pop.TotalFitness = 0
	for _, p := range pop.Phenotypes {
		pop.TotalFitness += p.Fitness
	}
}
