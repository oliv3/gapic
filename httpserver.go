package main

import (
	"fmt"
	"net/http"
	"strconv"
)

// Constants for MIME types.
const (
	CSS = iota
	JS
	MP4
	PNG
)

var contentTypes = map[int]string{
	CSS: "text/css",
	JS:  "application/javascript",
	MP4: "video/mp4",
	PNG: "image/png",
}

type static struct {
	file        string
	contentType int
}

var staticFiles = [12]static{
	{file: "www/js/jquery-3.6.0.min.js", contentType: JS},
	{file: "www/js/jquery-ui.min.js", contentType: JS},
	{file: "www/css/jquery-ui.min.css", contentType: CSS},
	{file: "www/js/websocket.js", contentType: JS},
	{file: "www/js/main.js", contentType: JS},
	{file: "www/js/stats.js", contentType: JS},
	{file: "current.png", contentType: PNG},
	{file: "gapic.mp4", contentType: MP4},
	{file: "www/css/gapic.css", contentType: CSS},
	{file: "www/css/videopopup.css", contentType: CSS},
	{file: "www/js/videopopup.js", contentType: JS},
	{file: "www/js/moment.min.js", contentType: JS},
}

func initHttp() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		serveFile("www/index.html", w, r)
	})
	for _, file := range staticFiles {
		func(f static) {
			http.HandleFunc("/"+f.file, func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Type", contentTypes[f.contentType])
				serveFile(f.file, w, r)
			})
		}(file)
	}
	http.HandleFunc("/replay", serveImage)
}

func serveFile(file string, w http.ResponseWriter, r *http.Request) {
	if r.Method != "GET" {
		http.Error(w, "Method not allowed", http.StatusMethodNotAllowed)
		return
	}
	http.ServeFile(w, r, file)
}

func serveImage(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	generation, err := strconv.Atoi(query.Get("generation"))
	if nil != err {
		panic(err.Error())
	}
	filename := fmt.Sprintf("out/out-%09d.png", generation)
	serveFile(filename, w, r)
	w.Header().Add("Content-Type", contentTypes[PNG])
}
